import React, { useMemo, useRef } from 'react';
import Todo from '../Todo/Todo';
import styles from './TodoList.module.scss';

import { TodoType } from '../../@types/types';


export interface ITodoList {
  todos: TodoType[],
  handleTodoAdd: (title: string) => void;
  handleTodoDelete: (id: number) => void;
  handleTodoEdit: (id: number, title: string) => void;
  handleTodoStatusChange: (id: number) => void;
}

const TodoList: React.FC<ITodoList> = ({ todos, handleTodoDelete, handleTodoAdd, handleTodoEdit, handleTodoStatusChange }) => {
  const todosView = useMemo(() => todos.map((todo) =>
    <Todo
      key={todo.id}
      handleTodoEdit={(title) => handleTodoEdit(todo.id, title)}
      handleTodoStatusChange={() => handleTodoStatusChange(todo.id)}
      handleTodoDelete={() => handleTodoDelete(todo.id)}
      {...todo} />), [todos]);
  const inputRef = useRef(null);

  const addTodo = (e) => {
    e.preventDefault()
    if (inputRef.current.value) {
      handleTodoAdd(inputRef.current.value)
      inputRef.current.value = ""
    }
  }

  return (
    <div className={styles.todoList}>
      {todosView}
      <form className={styles.addNewItem}>
        <input ref={inputRef} />
        <button type="submit" className={styles.button} onClick={addTodo} >
          Добавить задание
        </button>
      </form>
    </div>
  );
};

export default TodoList;
