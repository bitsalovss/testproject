import React, { useRef } from 'react';
import styles from './Day.module.scss';


export interface IDay {
  id?: number;
  title: string;
  handleGroupAdd: (title: string) => void;
}


const Day: React.FC<IDay> = ({ children, title, handleGroupAdd }) => {
  const inputRef = useRef(null)

  const addNewGroupHandler = (e) => {
    e.preventDefault()
    if (inputRef.current.value) {
      handleGroupAdd(inputRef.current.value)
      inputRef.current.value = ""
    }
  }

  return (
    <div className={styles.day}>
      <div className={styles.dayTitleContainer}>
        <h3 className={styles.dayTitle}>{title}</h3>
      </div >
      {children}
      <form className={styles.addNewItem}>
        <input ref={inputRef} />
        <button type="submit" className={styles.button} onClick={addNewGroupHandler}>Добавить новую группу</button>
      </form>
    </div>);
};

export default Day;
