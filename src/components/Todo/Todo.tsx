import React, { useRef, useState } from 'react';
import { deleteIcon, doneIcon, editIcon, undoneIcon } from '../../assets/icons/icons';
import styles from './Todo.module.scss';

interface ITodo {
  id?: number,
  groupId: number,
  title: string,
  status: boolean,
  handleTodoEdit: (newTitle: string) => void,
  handleTodoStatusChange: () => void,
  handleTodoDelete: () => void,
}


const Todo: React.FC<ITodo> = ({ title, status, handleTodoDelete, handleTodoEdit, handleTodoStatusChange }) => {
  const [edit, setEdit] = useState(false);

  const editToggle = () => {
    setEdit(prev => !prev)
  }

  const editSubmitHandler = () => {
    if (editRef.current.value) {
      handleTodoEdit(editRef.current.value)
    }
    editToggle();
  }
  const editRef = useRef(null)
  return (
    <div className={styles.todo}>
      {edit ?
        <>
          <input placeholder={title} ref={editRef} />
          <div className={styles.actionButtons} >
            <img className={styles.button} onClick={editSubmitHandler} src={doneIcon} alt="delete group" />
            <img className={styles.button} onClick={editToggle} src={undoneIcon} alt="delete group" />
          </div>
        </>
        :
        <>
          {status ?
            <span className={styles.done}>{title}</span>
            :
            <span >{title}</span>
          }
          <div className={styles.actionButtons}>
            {status ?
              <img className={styles.button} onClick={handleTodoStatusChange} src={undoneIcon} alt="delete group" /> :
              <img className={styles.button} onClick={handleTodoStatusChange} src={doneIcon} alt="delete group" />}
            <img className={styles.button} onClick={editToggle} src={editIcon} alt="edit" />
            <img className={styles.button} onClick={handleTodoDelete} src={deleteIcon} alt="edit" />
          </div>
        </>}

    </div>);
};

export default Todo;
