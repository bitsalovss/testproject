import React, { useRef, useState } from 'react';
import { deleteIcon, doneIcon, editIcon, undoneIcon } from '../../assets/icons/icons';
import styles from './Group.module.scss';


export interface IGroup {
  id?: number;
  title: string;
  handleGroupEdit: (title: string) => void;
  handleGroupDelete: () => void;
}

const Group: React.FC<IGroup> = ({ children, title, handleGroupEdit, handleGroupDelete }) => {
  const [edit, setEdit] = useState(false)

  const editToggle = () => {
    setEdit(prev => !prev)
  }

  const editSubmitHandler = () => {
    if (editRef.current.value) {
      handleGroupEdit(editRef.current.value)
    }
    editToggle();
  }

  const editRef = useRef(null);

  return (
    <div className={styles.group}>
      {edit ?
        <h3 className={styles.groupTitleContainer}>
          <input placeholder={title} ref={editRef} />
          <div className={styles.actionButtons}>
            <img className={styles.button} onClick={editSubmitHandler} src={doneIcon} alt="confirm" />
            <img className={styles.button} onClick={editToggle} src={undoneIcon} alt="cancel" />
          </div>
        </h3>
        :
        <h3 className={styles.groupTitleContainer}>
          <span className={styles.title}>{title}</span>
          <div className={styles.actionButtons}>
            <img className={styles.button} onClick={editToggle} src={editIcon} alt="edit" />
            <img className={styles.button} onClick={() => { handleGroupDelete() }} src={deleteIcon} alt="delete" />
          </div>
        </h3>
      }
      {children}
    </div>
  );
};

export default Group;
