export type TodoType = {
  id?: number
  title: string
  groupId: number
  status: boolean
}

export type GroupType = {
  id?: number
  title: string
  dayId: number
  todoIds: number[]
}

export type DayType = {
  id?: number
  title: string
  groupIds: number[]
}

export type StateType = {
  days: DayType[]
  groups: GroupType[]
  todos: TodoType[]
}


export interface PayloadType {
  todo?: Partial<TodoType>;
  group?: Partial<GroupType>;
}

export type ActionType = {
  type: 'TODO_ADD' | 'TODO_TITLE_CHANGE' | 'TODO_STATUS_CHANGE' | 'TODO_DELETE' | 'GROUP_ADD' | 'GROUP_TITLE_CHANGE' | 'GROUP_DELETE'
  payload: PayloadType
}