import { useCallback, useReducer } from "react";
import { StateType, ActionType, TodoType, GroupType } from "../@types/types";

const ENTITY_LATEST_ID = {
  day: 1,
  group: 1,
  todo: 1
}

const DUMMY_DATA = {
  days: [
    { id: 1, title: "Сегодня", groupIds: [ENTITY_LATEST_ID.group] },
    { id: 2, title: "Завтра", groupIds: [ENTITY_LATEST_ID.group] },
    { id: 3, title: "Послезавтра", groupIds: [ENTITY_LATEST_ID.group] },
  ],
  groups: [
    { id: ENTITY_LATEST_ID.group, title: 'Дома', dayId: ENTITY_LATEST_ID.day, todoIds: [ENTITY_LATEST_ID.todo] },
  ],
  todos: [
    { id: ENTITY_LATEST_ID.todo, title: 'Купить продукты', groupId: ENTITY_LATEST_ID.group, status: false }
  ],
}

const findElementById = (list, elementId) => {
  return list.find(({ id }) => id === elementId);
}

const dataChangeHandler = (state: StateType, action: ActionType): StateType => {
  const { todo: payloadTodo, group: payloadGroup } = action.payload;
  const group = findElementById(state.groups, payloadGroup?.id);
  const todo = findElementById(state.todos, payloadTodo?.id);
  switch (action.type) {
    case "TODO_ADD": {
      payloadTodo.id = ++ENTITY_LATEST_ID.todo;
      payloadTodo.status = false;
      return {
        ...state,
        todos: [...state.todos, payloadTodo as TodoType],
        groups: state.groups.map(element => element.id !== payloadTodo.groupId ? element : { ...element, todoIds: [...element.todoIds, payloadTodo.id] })
      }
    }
    case "TODO_TITLE_CHANGE": {
      return {
        ...state,
        todos: state.todos.map(todo => todo.id === payloadTodo.id ? { ...todo, title: payloadTodo.title } : todo),
      }
    }
    case "TODO_STATUS_CHANGE": {
      return {
        ...state,
        todos: state.todos.map(todo => todo.id === payloadTodo.id ? { ...todo, status: !todo.status } : todo),
      }
    }
    case "TODO_DELETE": {
      return {
        ...state,
        todos: state.todos.filter(todo => todo.id !== payloadTodo.id),
        groups: state.groups.map(element => element.id === todo.groupId ? { ...element, todoIds: element.todoIds.filter((id) => id !== todo.id) } : element)
      }
    }
    case "GROUP_ADD": {
      payloadGroup.id = ++ENTITY_LATEST_ID.group;
      payloadGroup.todoIds = []
      return {
        ...state,
        groups: [...state.groups, payloadGroup as GroupType],
        days: state.days.map(element => element.id === payloadGroup.dayId ? { ...element, groupIds: [...element.groupIds, payloadGroup.id] } : element)
      }
    }
    case "GROUP_TITLE_CHANGE": {
      return {
        ...state,
        groups: state.groups.map(groups => groups.id === payloadGroup.id ? { ...groups, title: payloadGroup.title } : groups),
      }
    }
    case "GROUP_DELETE": {
      return {
        ...state,
        groups: state.groups.filter(group => group.id !== payloadGroup.id),
        days: state.days.map(element => element.id === group.dayId ? { ...element, groupIds: element.groupIds.filter((id) => { id !== payloadGroup.id }) } : element),
        todos: state.todos.filter(t => t.groupId !== payloadGroup.id)
      }
    }
    default: {
      return state;
    }
  }
}

export const useAppState = (): [state: StateType, stateChangeActions: StateChangeActions] => {
  const [state, dispatch] = useReducer(dataChangeHandler, DUMMY_DATA);

  const addTodo = useCallback((groupId: number, title: string) => {
    dispatch({ type: 'TODO_ADD', payload: { todo: { title, groupId } } })
  }, [dispatch])
  const editTodo = useCallback((id: number, title: string) => {
    dispatch({ type: "TODO_TITLE_CHANGE", payload: { todo: { id, title } } })
  }, [dispatch])
  const changeTodoStatus = useCallback((id: number) => {
    dispatch({ type: "TODO_STATUS_CHANGE", payload: { todo: { id } } })
  }, [dispatch]);
  const deleteTodo = useCallback((id: number) => {
    dispatch({ type: 'TODO_DELETE', payload: { todo: { id } } })
  }, [dispatch])
  const addGroup = useCallback((dayId: number, title: string) => {
    dispatch({ type: 'GROUP_ADD', payload: { group: { title, dayId } } })
  }, [dispatch])
  const editGroup = useCallback((id: number, title: string) => {
    dispatch({ type: "GROUP_TITLE_CHANGE", payload: { group: { id, title } } })
  }, [dispatch])
  const deleteGroup = useCallback((id: number) => {
    dispatch({ type: "GROUP_DELETE", payload: { group: { id } } })
  }, [dispatch])

  return [state, {
    addTodo,
    editTodo,
    changeTodoStatus,
    deleteTodo,
    addGroup,
    editGroup,
    deleteGroup,
  }]
};

export interface StateChangeActions {
  addTodo: (groupId: number, title: string) => void;
  editTodo: (id: number, title: string) => void;
  changeTodoStatus: (id: number) => void;
  deleteTodo: (id: number) => void;
  addGroup: (dayId: number, title: string) => void;
  editGroup: (id: number, title: string) => void;
  deleteGroup: (id: number) => void;
}



