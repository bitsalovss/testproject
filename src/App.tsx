import React, { useCallback } from 'react';
import Day from './components/Day/Day';
import Group from './components/Group/Group';
import TodoList from './components/TodoList/TodoList';
import { useAppState } from './hooks/useAppState';


const App: React.FC = () => {
  const [state, {
    addTodo,
    editTodo,
    changeTodoStatus,
    deleteTodo,
    addGroup,
    editGroup,
    deleteGroup,
  }] = useAppState();


  return (
    <div>
      {state.days.map(day => (
        <Day key={day.id} {...day} handleGroupAdd={(title) => addGroup(day.id, title)}>
          {state.groups.map(group => group.dayId === day.id &&
            <Group key={group.id}
              {...group}
              handleGroupEdit={(title) => { editGroup(group.id, title) }}
              handleGroupDelete={() => { deleteGroup(group.id) }}
            >
              <TodoList todos={state.todos.filter(todo => todo.groupId === group.id)}
                handleTodoAdd={(title) => addTodo(group.id, title)}
                handleTodoDelete={deleteTodo}
                handleTodoEdit={editTodo}
                handleTodoStatusChange={changeTodoStatus} />
            </Group>
          )}
        </Day>
      ))}
    </div>
  );
};

export default App;
